<?php
define('API_PROJECT_INDEX_PATH', 'public://projects/module_usage.html');
define('API_PROJECT_INDEX_URL', 'http://drupal.org/project/usage');
define('API_PROJECT_INDEX_MAX', 1000);

function api_project_update_index_batch() {
  $batch = array(
    'title' => t('Process project index'),
    'operations' => array(),
    'finished' => 'api_project_update_index_batch_finished',
    'file' => drupal_get_path('module', 'api_project_update') . '/api_project_update.manager.inc',
  );
  
  $index = api_project_update_index_fetch();
  $tasks = round(count($index) / API_PROJECT_INDEX_MAX);
  for($i = 0; $i < $tasks; $i++) {
    $batch['operations'][] = array('api_project_update_index_batch_step', array(($i * API_PROJECT_INDEX_MAX)));
  }
  
  return $batch;
}

function api_project_update_index_batch_step($offset, &$context) {
  
  $index = api_project_update_index_fetch();
  $context['message'] = 'Running from offset ' . $offset;
  
  api_project_update_index_process($index, $offset, API_PROJECT_INDEX_MAX);
}

function api_project_update_index_batch_finished($success, $results, $operations) {
  drupal_set_message('Finished loading the index.');
}

function api_project_update_index_fetch() {
    // Load from file.
  if (file_exists(API_PROJECT_INDEX_PATH)) {
    $data = file_get_contents(API_PROJECT_INDEX_PATH);
  } 
  // Renew from online.
  else {   
    $http_result = drupal_http_request(API_PROJECT_INDEX_URL);
    if (isset($http_result->data)) {
      $data = $http_result->data;
      file_unmanaged_save_data($data, API_PROJECT_INDEX_PATH);
    }
  }
  
  try {
    // Load the module index into SimpleXMLElement.
    // Suppresse errors.
    libxml_use_internal_errors(true);
    $doc = new DOMDocument();
    $doc->strictErrorChecking = FALSE;
    $doc->loadHTML($data);
    $xml = simplexml_import_dom($doc);
  } 
  catch (Exception $e) {
    // SimpleXMLElement::__construct produces an E_WARNING error message for
    // each error found in the XML data and throws an exception if errors
    // were detected. Catch any exception and return failure (NULL).
    return;
  }
  
  // Find the ordered list with projects.
  $ol_element = $xml->xpath("//table[@id='project-usage-all-projects']/tbody//tr");
  
  // Clean up memory.
  unset($doc, $xml, $data);
  
  return $ol_element;
}

/**
 * Retrieve index from usage.
 * 
 * $index is generated api_project_update_index_fetch().
 * 
 * @return array   
 */
function api_project_update_index_process($index, $offset = 0, $max = NULL) {
  
  $count = 0;
  for($i = $offset; $i < count($index); $i++) {
    $li = $index[$i];
    $tds = $li->children();

    $project = array();
    
    $project['rank'] = (int) $tds[0];
    $project['name'] = (string) $tds[1]->a;
    $project_shortname = (string) $tds[1]->a->attributes()->{'href'};
    $project['shortname'] = str_replace('/project/usage/', '', $project_shortname);

    api_project_update_save_project($project);
    
    $count++;
    if (isset($max) && $count == $max) {
      break;
    }
  }
  unset($query, $project_exists, $project_name, $project_rank, $project_shortname);
}

// Retrieve release information.

/**
 * Save project information.
 * 
 * @param array $project Array keyed with shortname, name, and rank.
 */
function api_project_update_save_project($project) {
  
  // Check if is exists.
  $query = db_select('api_project_reference', 'r');

  $query->fields('r', array('uri', 'name', 'weight'));
  $query->condition('uri', $project['shortname'], '=');
  $project_exists = $query->execute()->fetchAssoc();

  if (!empty($project_exists)) {

    //dpm($project_exists, 'exists');
    
    if ($project_exists['name'] == $project['name'] && $project_exists['weight'] == $project['rank']) {
      return;
    }
    
    // Update the record.
    $update = db_update('api_project_reference');
    $update->fields(array(
      'name' => $project['name'],
      'weight' => $project['rank'],
    ))
    ->condition('uri', $project['shortname'], '=')
    ->execute();
  }
  else {

    // Insert the record.
    $insert = db_insert('api_project_reference');
    $insert->fields(array(
      'uri' => $project['shortname'],
      'name' => $project['name'],
      'weight' => $project['rank'],
    ))->execute();
  }
}

// Save release information.

/**
 * Return the release information from updates.drupal.org.
 * 
 * This method returns the release information of a project in a structured
 * array back.
 * 
 * - $project
 * - name The project name you want to retrieve.
 * - api_version The core version for that project.
 * 
 * @param array $project
 * @return array
 */
function api_project_update_fetch_project($project) {
  module_load_include('inc', 'update', 'update.fetch');  

  $name = $project['name'];
  $url = _update_get_fetch_url_base(NULL);
  $url .= '/' . $name . '/' . $project['api_version'];
  
  $url = _update_build_fetch_url($project);

  $xml = drupal_http_request($url);
  if (!isset($xml->error) && isset($xml->data)) {
    $data = $xml->data;
  } else {
    drupal_set_message($xml->error, 'error');
  }

  if (isset($data)) {
  try {
    $xml = new SimpleXMLElement($data);
  }
  catch (Exception $e) {
    // SimpleXMLElement::__construct produces an E_WARNING error message for
    // each error found in the XML data and throws an exception if errors
    // were detected. Catch any exception and return failure (NULL).
    return;
  }
  } else {
    return;
  }

  $project = array(
    'uri' => (string) $xml->short_name,
    'api_version' => (string) $xml->api_version,
    'recommended_major' => (string) $xml->recommended_major,
    'supported_majors' => (string) $xml->supported_majors,
    'releases' => array(),
  );

  $releases = ((array)$xml->releases);
  $releases = $releases['release'];
  foreach($releases as $release) {
    $releases =& $project['releases'];
    $tag = (string) $release->tag;
    
    $elements = array(
      'tag',
      'version_major',
      'version_patch',
      'version_extra',
      'download_link',
    );

    foreach ($elements as $element) {
      if (isset($release->{$element})) {
        $releases[$tag][$element] = (string) $release->{$element};
      }
    }
  }
  
  return $project;
}

function api_project_update_file_get($url) {
  $parsed_url = parse_url($url);
  $remote_schemes = array('http', 'https', 'ftp', 'ftps', 'smb', 'nfs');
  if (!in_array($parsed_url['scheme'], $remote_schemes)) {
    // This is a local file, just return the path.
    return drupal_realpath($url);
  }

  // Check the cache and download the file if needed.
  $cache_directory = _api_project_update_manager_cache_directory();
  $local = $cache_directory . '/' . drupal_basename($parsed_url['path']);

  // @see update_manager_file_get(). This uses function for stale file.
  // Don't know why that is neccecary here.
  if (!file_exists($local)) {
    return system_retrieve_file($url, $local, FALSE, FILE_EXISTS_REPLACE);
  }
  else {
    return $local;
  }
 }
 
 function _api_project_update_manager_cache_directory($create = TRUE) {
  $directory = &drupal_static(__FUNCTION__, '');
  if (empty($directory)) {
    // @see _update_manager_cache_direcory().
    // That method uses a unique id for the dir.
    $directory = 'temporary://api-project-update-cache'; 
    if ($create && !file_exists($directory)) {
      mkdir($directory);
    }
  }
  return $directory;
}